package org.exite.archive.mq;

import org.exite.archive.Config;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.Response;
import redis.clients.jedis.Transaction;
import redis.clients.jedis.exceptions.JedisConnectionException;

import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.util.List;

import static java.lang.Thread.sleep;

/**
 * Created by alex on 10.08.17.
 */
public class RedisWorker {

    private static String PID;
    private Jedis server;
    private String worker_name;
    private String main_prefix = "mq:";
    private String queue_prefix = main_prefix + "queue:";
    private String process_prefix = main_prefix + "process:";
    private String docs_prefix = main_prefix + "docs:";
    private String doc_path_prefix = main_prefix + "docspath:";
    private String log_prefix = main_prefix + "docslog:";
    private String next_doc_id = main_prefix + "next_doc_id";
    private String j_writer_queue = queue_prefix + "j_writer_esf";
    private int expire_time = 600;
    private int expire_log_time = 10000;
    private int number = 0;

    public RedisWorker() throws Exception {
        server = new Jedis(Config.redisHost, Config.redisPort, 0);
        connect();
        PID = getPID();
    }

    public RedisWorker(String current_worker) {
        server = new Jedis(Config.redisHost, Config.redisPort, 0);
        server.connect();
        worker_name = current_worker;
        PID = getPID();
    }

    private void connect() throws Exception {
        try {
            server.connect();
        } catch (JedisConnectionException ex) {
            if (number < 3) {
                sleep(10000);
                number++;
                connect();
            }
        }
    }

    public void close() {
        server.disconnect();
    }

    public void isConnected() throws Exception {
        boolean redisCheck = false;
        while (!redisCheck) {
            try {
                server.ping();
                redisCheck = true;
            } catch (Exception e) {
                sleep(10000);
                reconnect();
            }
        }
    }

    public void reconnect() throws Exception {
        if (server.isConnected())
            server.disconnect();

        while(!server.isConnected()) {
            try {
                server = new Jedis(Config.redisHost, Config.redisPort, 0);
                server.connect();
            } catch (Exception e) {
                if (!server.isConnected()) {
                    sleep(10000);
                }
            }
        }
    }

    public long getQueueDocCount() throws Exception {
        isConnected();
        return getQueueDocCount(worker_name);
    }

    public long getQueueDocCount(String worker_name) throws Exception {
        isConnected();
        return server.llen(queue_prefix + worker_name);
    }

    public void setExpire(int file_id) throws Exception {
        isConnected();
        server.setex(doc_path_prefix + file_id, expire_time, getTime());
    }

    public String getExpire(long file_id) throws Exception {
        isConnected();
        return server.get(doc_path_prefix + file_id);
    }

    public String getExpire(String fileUUID) throws Exception {
        isConnected();
        return server.get(doc_path_prefix + fileUUID);
    }

    public MQRecord getRecord(String worker_name) throws Exception {
        isConnected();
        MQRecord record = null;
        try {
            List<String> mq_doc_ids = server.blpop(1, queue_prefix + worker_name);

            if (mq_doc_ids != null && mq_doc_ids.size() > 0) {
                String mq_doc_id = mq_doc_ids.get(1);
                Transaction transaction = server.multi();
                transaction.hset(process_prefix + "docid", PID, mq_doc_id);
                transaction.hset(process_prefix + "time", PID, getTime());
                transaction.hset(process_prefix + "worker", PID, worker_name);


                Response<String> content = transaction.hget(docs_prefix + mq_doc_id, "xml");
                Response<String> user_id = transaction.hget(docs_prefix + mq_doc_id, "uid");
                Response<String> subservice = transaction.hget(docs_prefix + mq_doc_id, "subservice");
                Response<String> folder_name = transaction.hget(docs_prefix + mq_doc_id, "folder");
                Response<String> folder_id = transaction.hget(docs_prefix + mq_doc_id, "folder_id");
                Response<String> file_name = transaction.hget(docs_prefix + mq_doc_id, "file");
                Response<String> file_id = transaction.hget(docs_prefix + mq_doc_id, "file_id");
                Response<String> doc_type = transaction.hget(docs_prefix + mq_doc_id, "doc_type");
                Response<byte[]> sign = transaction.hget((docs_prefix + mq_doc_id).getBytes(), "sign".getBytes());
                Response<String> sign_info = transaction.hget(docs_prefix + mq_doc_id, "signinfo");
                Response<String> sign_count = transaction.hget(docs_prefix + mq_doc_id, "signcount");
                Response<String> timestamp = transaction.hget(docs_prefix + mq_doc_id, "timestamp");
                Response<String> cript = transaction.hget(docs_prefix + mq_doc_id, "cript");
                Response<String> docFlowUUID = transaction.hget(docs_prefix + mq_doc_id, "doc_flow_guid");
                Response<String> FileUUID = transaction.hget(docs_prefix + mq_doc_id, "doc_file_uuid");
                Response<String> ProcessingRule = transaction.hget(docs_prefix + mq_doc_id, "processing_rule");
                Response<String> evoDocID = transaction.hget(docs_prefix + mq_doc_id, "evoDocID");
                Response<String> edsDocUUID = transaction.hget(docs_prefix + mq_doc_id, "edsDocUUID");

                transaction.exec();

                record = new MQRecord();
                record.setMqid(mq_doc_id);
                record.setFileBodyBase64(content.get());
                record.setUserID(Integer.parseInt(user_id.get()));
                record.setFolderName(folder_name.get());
                record.setFolderID(Integer.parseInt(folder_id.get()));
                record.setFileName(file_name.get());
                record.setFileID(Long.parseLong(file_id.get()));
                record.setSignBody(sign.get());
                record.setDocType(doc_type.get());
                record.setSubservice(Integer.parseInt(subservice.get()));
                if (sign_info.get() != null)
                    record.setSignInfo(sign_info.get());

                if (sign_count.get() != null)
                    record.setSignCount(Integer.parseInt(sign_count.get()));

                if (timestamp.get() != null)
                    record.setTimestamp(timestamp.get());

                if (cript.get() != null)
                    record.setCript(cript.get());

                if (FileUUID.get() != null )
                    record.setFileUUID(FileUUID.get());

                if (docFlowUUID.get() != null)
                    record.setDocFlowGUID(docFlowUUID.get());

                if (ProcessingRule.get() != null)
                    record.setProcessingRule(ProcessingRule.get());

                if (evoDocID.get() != null)
                    record.setEvoDocID(Long.parseLong(evoDocID.get()));

                if (edsDocUUID.get() != null)
                    record.setEdsDocUUID(edsDocUUID.get());

                return record;
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
            throw e;
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("Cant get document from MQ server. Cause: " + e.getMessage() + ". Record: " + record);
        }
        throw new Exception("Cant get document from MQ server");
    }

    public MQRecord getRecord() throws Exception {
        return getRecord(worker_name);
    }

    public String moveToWriter(int userID, String fileName, String folderName, int subservice, String content) throws Exception {
        isConnected();
        final String new_doc_id = Long.toString(server.incr(next_doc_id));

        Transaction transaction = server.multi();
        transaction.hsetnx(docs_prefix + new_doc_id, "user_id", Integer.toString(userID));
        transaction.hsetnx(docs_prefix + new_doc_id, "file_name", fileName);
        transaction.hsetnx(docs_prefix + new_doc_id, "folder_name", folderName);
        transaction.hsetnx(docs_prefix + new_doc_id, "file_content", content);
        transaction.hsetnx(docs_prefix + new_doc_id, "subservice", Integer.toString(subservice));

        // j_writer_queue
        transaction.rpush(j_writer_queue, new_doc_id);
        transaction.exec();

        return new_doc_id;
    }

    public void clear(final String doc_id) throws Exception {
        isConnected();
        if (doc_id != null) {
            server.del(docs_prefix + doc_id);
            server.hdel(process_prefix + "docid", PID);
            server.hdel(process_prefix + "time", PID);
            server.hdel(process_prefix + "worker", PID);
            server.rpush(log_prefix + doc_id, getDateTime() + "end doc processing");
            server.expire(log_prefix + doc_id, expire_log_time);
        }
    }

    private String getDateTime() {
        String sDate = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
                .format(java.util.Calendar.getInstance().getTime());
        return "[" + sDate + "] ";
    }

    private String getTime() {
        String Now = Long.toString(System.currentTimeMillis());
        return Now.substring(0, 10);
    }

    private String getPID() {
        RuntimeMXBean bean = ManagementFactory.getRuntimeMXBean();
        String PID = bean.getName().split("@")[0];
        return PID;
    }
}
