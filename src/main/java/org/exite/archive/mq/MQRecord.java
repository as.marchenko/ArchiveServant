package org.exite.archive.mq;

import org.apache.commons.codec.binary.Base64;

/**
 * Created by alex on 10.08.17.
 */
public class MQRecord {

    private String mqid;
    private int user_id;
    private int subservice;
    private String user_name;
    private long file_id;
    private int folder_id;
    private String folder_name;
    private String file_name;
    private byte[] file_body;
    private byte[] sign_body;
    private String file_body_base64;
    private int is_archive;
    private String docType;
    private int signCount;
    private String signInfo;
    private String timestamp;
    private String cript;
    private String fileUUID;
    private String docFlowGUID;
    private long evoDocID;
    private String edsDocUUID;

    private String processingRule;

    public int getSignCount() {
        return signCount;
    }

    public void setSignCount(int signCount) {
        this.signCount = signCount;
    }

    public String getSignInfo() {
        return signInfo;
    }

    public void setSignInfo(String signInfo) {
        this.signInfo = signInfo;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }

    public String getDocType() {
        return this.docType;
    }

    public int getSubservice() {
        return subservice;
    }

    public void setSubservice(int subservice) {
        this.subservice = subservice;
    }

    public void setSubservice(final String subservice) throws Exception {

        if(subservice.equals("ua"))
            this.subservice = 1;
        else if(subservice.equals("ru"))
            this.subservice = 2;
        else if(subservice.equals("kz"))
            this.subservice = 11;
        else
            throw new Exception("Unknown subservice");
    }

    public int getUserID() {
        return user_id;
    }

    public void setUserID(int user_id) {
        this.user_id = user_id;
    }

    public String getUserName() {
        return user_name;
    }

    public void setUserName(String user_name) {
        this.user_name = user_name;
    }

    public long getFileID() {
        return file_id;
    }

    public void setFileID(long file_id) {
        this.file_id = file_id;
    }

    public int getFolderID() {
        return folder_id;
    }

    public void setFolderID(int folder_id) {
        this.folder_id = folder_id;
    }

    public String getFolderName() {
        return folder_name;
    }

    public void setFolderName(String folder_name) {
        this.folder_name = folder_name;
    }

    public String getFileName() {
        return file_name;
    }

    public void setFileName(String file_name) {
        this.file_name = file_name;
    }

    public byte[] getFileBody() {
        return file_body;
    }

    public void setFileBody(byte[] file_body) {
        this.file_body = file_body;
        this.file_body_base64 = Base64.encodeBase64String(file_body);
    }

    public byte[] getSignBody() {
        if (sign_body != null && sign_body.length > 0) {
            return sign_body;
        } else {
            return null;
        }

    }

    public String getSignBodyString() {
        if (sign_body != null && sign_body.length > 0) {
            return new String(sign_body);
        } else {
            return null;
        }
    }

    public void setSignBody(byte[] sign_body) {
        this.sign_body = sign_body;
    }

    public String getFileBodyBase64() {
        return file_body_base64;
    }

    public void setFileBodyBase64(String file_body_base64) {
        this.file_body_base64 = file_body_base64;
        this.file_body = Base64.decodeBase64(file_body_base64);
    }

    public int getIsArchive() {
        return is_archive;
    }

    public void setIsArchive(int is_archive) {
        this.is_archive = is_archive;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }


    public String getCript() {
        return cript;
    }

    public void setCript(String cript) {
        this.cript = cript;
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("MQRecord [user_id=").append(user_id).append(", subservice=")
                .append(subservice).append(", file_id=").append(file_id).append(", folder_id=").append(folder_id)
                .append(", folder_name=").append(folder_name).append(", file_name=").append(file_name).append(", file_body=")
                .append(file_body).append(", sign_body=").append(sign_body);
        if (signCount > 0)
            sb.append(", signCount=").append(signCount);
        if (signInfo != null)
            sb.append(", signInfo=").append(signInfo);
        if (cript != null)
            sb.append(", cript=").append(cript.getBytes());

        sb.append(", timestamp=").append(timestamp);
        return sb.toString();
    }

    public String getFileUUID() {
        return fileUUID;
    }

    public void setFileUUID(String fileUUID) {
        this.fileUUID = fileUUID;
    }

    public void setDocFlowGUID(String docFlowGUID) {
        this.docFlowGUID = docFlowGUID;
    }

    public String getDocFlowGUID() {
        return docFlowGUID;
    }

    public String getProcessingRule() {
        return processingRule;
    }

    public void setProcessingRule(String processingRule) {
        this.processingRule = processingRule;
    }


    public long getEvoDocID() {
        return evoDocID;
    }

    public void setEvoDocID(long evoDocID) {
        this.evoDocID = evoDocID;
    }

    public String getEdsDocUUID() {
        return edsDocUUID;
    }

    public void setEdsDocUUID(String edsDocUUID) {
        this.edsDocUUID = edsDocUUID;
    }

    public String getMqid() {
        return mqid;
    }

    public void setMqid(String mqid) {
        this.mqid = mqid;
    }
}
