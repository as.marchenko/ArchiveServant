package org.exite.archive;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by alex on 10.08.17.
 */
public class Config {

    public static String invexUrl;
    public static String invexUser;
    public static String invexPass;

    public static String arcUrl;
    public static String arcUser;
    public static String arcPass;

    public static String dbproUrl;
    public static String dbproUser;
    public static String dbproPass;

    public static String redisHost;
    public static int redisPort;
    static String redisQueue;

    static int poolSize;

    Config(String configFile) {
        this.loadConf(configFile);
    }

    private void loadConf(String filename) {
        Properties prop = new Properties();

        try {
            prop.load(new FileInputStream(filename));
            invexUrl = prop.getProperty("invexUrl");
            invexUser = prop.getProperty("invexUser");
            invexPass = prop.getProperty("invexPass");

            arcUrl = prop.getProperty("arcUrl");
            arcUser = prop.getProperty("arcUser");
            arcPass = prop.getProperty("arcPass");

            dbproUrl = prop.getProperty("dbproUrl");
            dbproUser = prop.getProperty("dbproUser");
            dbproPass = prop.getProperty("dbproPass");

            redisHost = prop.getProperty("redisHost");
            redisPort = Integer.parseInt(prop.getProperty("redisPort"));
            redisQueue = prop.getProperty("redisQueue");

            poolSize = Integer.parseInt(prop.getProperty("poolSize"));

        } catch (IOException var4) {
            var4.printStackTrace();
        }
    }
}
