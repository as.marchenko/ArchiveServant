package org.exite.archive.docs;

import com.thoughtworks.xstream.annotations.XStreamAlias;

public class IZVPOLInfo {

    @XStreamAlias("�����������")
    public RelativeFileInfo relativeFile;

    @Override
    public String toString() {
        return "IZVPOLInfo [relativeFile=" + relativeFile + "]";
    }
}