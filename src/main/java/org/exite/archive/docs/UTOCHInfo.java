package org.exite.archive.docs;

import com.thoughtworks.xstream.annotations.XStreamAlias;

public class UTOCHInfo {

    @XStreamAlias("�����������")
    public RelativeFileInfo relativeFile;

    @XStreamAlias("�������������")
    public String text;

    @Override
    public String toString() {
        return "UTOCHInfo [relativeFile=" + relativeFile + "]";
    }
}