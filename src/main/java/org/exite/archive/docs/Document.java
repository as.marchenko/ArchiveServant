package org.exite.archive.docs;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * Created by alex on 10.08.17.
 */
public class Document {

    @XStreamAlias("����������")
    public IZVPOLInfo izvpolInfo;

    @XStreamAlias("����������")
    public UTOCHInfo utochInfo;

    @XStreamAlias("���������")
    public UPDPokInfo updPokInfo;

    @XStreamAlias("������4")
    public INFPOKSODFHZH4 sodfhzh4;

    @Override
    public String toString() {
        return "Document{" +
                "izvpolInfo=" + izvpolInfo +
                ", utochInfo=" + utochInfo +
                ", updPokInfo=" + updPokInfo +
                ", sodfhzh4=" + sodfhzh4 +
                '}';
    }
}
