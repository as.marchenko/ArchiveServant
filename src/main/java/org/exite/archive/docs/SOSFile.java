package org.exite.archive.docs;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

/**
 * Created by alex on 10.08.17.
 */
public class SOSFile {

    @XStreamAlias("������")
    @XStreamAsAttribute
    public String id;

    @XStreamAlias("��������")
    public Document document;

    @XStreamAlias("������")
    public Document infpok;

    @Override
    public String toString() {
        return "SOSFile{" +
                "id='" + id + '\'' +
                ", document=" + document +
                ", infpok=" + infpok +
                '}';
    }
}
