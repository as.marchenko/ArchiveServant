package org.exite.archive.docs;


import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

public class UPDPokInfo {

    @XStreamAlias("�����������")
    @XStreamAsAttribute
    public String name;

    @Override
    public String toString() {
        return "UPDPokInfo{" +
            "name='" + name + '\'' +
            '}';
    }
}
