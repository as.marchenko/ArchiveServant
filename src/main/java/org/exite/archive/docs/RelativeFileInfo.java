package org.exite.archive.docs;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

public class RelativeFileInfo {

    @XStreamAlias("������������")
    @XStreamAsAttribute
    public String name;

    @XStreamAlias("������������")
    @XStreamAsAttribute
    public String reestrName;

    @XStreamAlias("����������")
    @XStreamAsAttribute
    public String specName;

    @Override
    public String toString() {
        return "RelativeFileInfo [name=" + name + ", reestrName=" + reestrName + ", specName=" + specName + "]";
    }
}