package org.exite.archive;

import org.exite.archive.db.DbConnectionsPool;
import org.exite.archive.mq.MQRecord;
import org.exite.archive.mq.RedisWorker;
import org.exite.archive.utils.Utils;
import org.exite.logging.LoggerManager;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by alex on 10.08.17.
 */
public class ArchiveServant {

    static RedisWorker mq;
    static DbConnectionsPool cons = new DbConnectionsPool();

    private static boolean execute = true;

    public static void main(String[] args) throws Exception {

        Utils.print("Application started.");
        if (args.length == 1) {
            new Config(args[0]);

            Utils.print("Init connections pool");
            mq = new RedisWorker(Config.redisQueue);
            cons.initConnections(Config.poolSize);

            final ExecutorService executor = Executors.newFixedThreadPool(Config.poolSize);
            while (execute) {
                if (mq.getQueueDocCount() > 0) {
                    final MQRecord record = mq.getRecord();
                    final Runnable worker = new Worker(record);
                    executor.execute(worker);
                } else {
                    Utils.print("queue empty");
                    sleep(20000);
                }
            }
            executor.shutdown();
            while (!executor.isTerminated()) {
                Thread.sleep(1000);
            }
        } else {
            Utils.print("forgot config file");
        }
    }

    private void registerShutdownHook(final Thread mainThread) {

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            Utils.print("Received EXIT_SIGNAL");
            try {
                execute = false;
                cons.releaseAll();
                mainThread.join();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                LoggerManager.resetFinally();
            }
        }));
    }

    private static void sleep(long time) {
        try {
            Thread.sleep(time);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
