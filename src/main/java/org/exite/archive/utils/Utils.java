package org.exite.archive.utils;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import com.thoughtworks.xstream.mapper.CannotResolveClassException;
import com.thoughtworks.xstream.mapper.MapperWrapper;

import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by alex on 10.08.17.
 */
public class Utils {

    public static void print(String text) {
        final String sDate = new SimpleDateFormat("yy/MM/dd HH:mm:ss").format(Calendar.getInstance().getTime());
        System.out.println("[" + sDate + "] " + text);
    }

    public static XStream initXstream(Class<?> aliasObject, String alias) {
        XStream xstream = new XStream(new DomDriver()) {
            @Override
            protected MapperWrapper wrapMapper(MapperWrapper next) {
                return new MapperWrapper(next) {
                    @Override
                    @SuppressWarnings("rawtypes")
                    public boolean shouldSerializeMember(Class definedIn, String fieldName) {
                        try {
                            return definedIn != Object.class || realClass(fieldName) != null;
                        } catch (CannotResolveClassException cnrce) {
                            return false;
                        }
                    }
                };
            }
        };

        xstream.autodetectAnnotations(true);
        xstream.setMode(XStream.NO_REFERENCES);
        xstream.alias(alias, aliasObject);
        return xstream;
    }

    public static void write2file(String path, byte[] content) throws Exception {
        FileOutputStream fos = new FileOutputStream(new File(path));
        fos.write(content);
        fos.close();
    }

//    public static void main(String[] args) throws Exception {
//        final byte[] zip = Files.readAllBytes(Paths.get("/home/alex/asd/archiveServant/example.zip"));
//        ZipHelper zh = new ZipHelper();
//        zh.prepareZip(zip);
//        zh.addFile("abra.xml", "abrabody".getBytes());
//        write2file("/home/alex/asd/archiveServant/example2.zip", zh.generate());
//    }


}
