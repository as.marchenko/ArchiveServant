package org.exite.archive.utils;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

/**
 * Created by alex on 11.08.17.
 */
public class ZipHelper {

    private Map<String, byte[]> files;

    public ZipHelper() {
        files = new HashMap<>();
    }

    public void prepareZip(byte[] zipBody) throws Exception {

        ZipInputStream zip = new ZipInputStream(new ByteArrayInputStream(zipBody));
        ZipEntry entry;
        ByteArrayOutputStream fileBody = new ByteArrayOutputStream();
        int data;
        while ((entry = zip.getNextEntry()) != null) {
            while ((data = zip.read()) != -1) {
                fileBody.write(data);
            }
            fileBody.close();
            zip.closeEntry();
            files.put(entry.getName(), fileBody.toByteArray());
            fileBody.reset();
        }
        zip.close();
    }

    public void addFile(final String name, final byte[] body) throws Exception {
        files.putIfAbsent(name, body);
    }

    public byte[] generate() throws Exception {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ZipOutputStream zip = new ZipOutputStream(new BufferedOutputStream(baos));
        for (final Map.Entry<String, byte[]> entry : files.entrySet()) {
            zip.putNextEntry(new ZipEntry(entry.getKey()));
            zip.write(entry.getValue());
            zip.closeEntry();
        }
        zip.close();
        return baos.toByteArray();
    }
}
