package org.exite.archive.utils;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.StringWriter;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Created by alex on 11.08.17.
 */
public class InvexPackager {

    public String filename;
    public String signname;

    public InvexPackager() {
        filename = generateName();
        signname = generateName();
    }

    public byte[] createDescription(final Map<String, String> xmldata) throws Exception {

        VelocityEngine ve = new VelocityEngine();
        Properties properties = new Properties();
        properties.setProperty("resource.loader", "file");
        properties.setProperty("file.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        properties.setProperty("file.resource.loader.path", "resources");
        properties.setProperty("file.resource.loader.cache", "true");
        properties.setProperty("file.resource.loader.modificationCheckInterval", "2");
        ve.init(properties);

        ve.init();
        VelocityContext context = new VelocityContext();
        StringWriter writer = new StringWriter();
        Template template;

        xmldata.put("filename", filename);
        xmldata.put("signname", signname);
        try  {
            context.put("data", xmldata);
            template = ve.getTemplate("desc.vm", "windows-1251");
            template.merge(context, writer);
        }  catch( Exception e )  {
            System.err.println("Exception getDescriptionTemplate: " + e.getMessage());
        }

        return writer.toString().getBytes();
    }

    public byte[] generatePacket(final byte[] description, final byte[] file, final byte[] sign) {
        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ZipOutputStream zip_out = new ZipOutputStream(new BufferedOutputStream(baos));

            zip_out.putNextEntry(new ZipEntry("packageDescription.xml"));
            zip_out.write(description);
            zip_out.closeEntry();

            zip_out.putNextEntry(new ZipEntry(filename));
            zip_out.write(file);
            zip_out.closeEntry();

            zip_out.putNextEntry(new ZipEntry(signname));
            zip_out.write(sign);
            zip_out.closeEntry();
            zip_out.close();
            baos.close();
            return baos.toByteArray();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("ZipCreationError: " + e.getMessage());
            return new byte[0];
        }
    }

    private String generateName() {
        final String uuid = UUID.randomUUID().toString();
        return uuid.replaceAll("-", "").toLowerCase() + ".bin";
    }
}
