package org.exite.archive;

import com.thoughtworks.xstream.XStream;
import org.exite.archive.db.ArchiveDB;
import org.exite.archive.db.ArchiveData;
import org.exite.archive.db.DbproDB;
import org.exite.archive.db.InvexDB;
import org.exite.archive.docs.SOSFile;
import org.exite.archive.mq.MQRecord;
import org.exite.archive.utils.InvexPackager;
import org.exite.archive.utils.Utils;
import org.exite.archive.utils.ZipHelper;

import java.io.ByteArrayInputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by alex on 10.08.17.
 */
public class Worker extends Thread {

    private MQRecord record;
    private XStream xStream;
    private InvexDB invexDB;
    private ArchiveDB arcDB;
    private DbproDB dbpro;

    Worker(final MQRecord record) {
        this.record = record;
        this.xStream = Utils.initXstream(SOSFile.class, "����");
        try {
            this.invexDB = ArchiveServant.cons.getInvexDB();
            this.arcDB = ArchiveServant.cons.getArcDB();
            this.dbpro = ArchiveServant.cons.getDbproDB();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        }
    }

    @Override
    public void run() {
        try {
            handleRecord();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
//                ArchiveServant.cons.releaseAll();
                ArchiveServant.cons.releaseInvexDB(invexDB);
                ArchiveServant.cons.releaseArcDB(arcDB);
                ArchiveServant.cons.releaseDbproDB(dbpro);
            } catch (Exception e) {
                e.printStackTrace();
                System.exit(0);
            }
        }
    }

    private void handleRecord() {
        try {
            print(record.getFileName());
            final DocInfo doc = getDocInfo();
            if (doc.getParentGUID() != null)
                doc.setExchangeGUID(invexDB.getExchangeGUID(doc.getParentGUID()));

            // get data from archive
            final ArchiveData arcData = arcDB.getArchiveData(doc.getExchangeGUID());

            if (arcData == null) {
                print("arcData not found. skip record");
                ArchiveServant.mq.clear(record.getMqid());
                return;
            }

            // prepare info for logging
            final StringBuilder sb = new StringBuilder();
            sb.append("docData: [exchangeID=").append(doc.getExchangeGUID()).append(", docID=").append(arcData.docid)
                    .append(", mqID=").append(record.getMqid()).append("]");

            print(sb.toString());
            sb.delete(0, sb.length());

            // create zip packet and send to invex
            final byte[] packet = createInvexPacket(doc);
            final long invexID = invexDB.saveInvexTransaction(doc, packet);
            if (invexID > 0)
                print("saved in sos_docs with id=" + invexID);
            else
                System.exit(0);

            // get zip from archive
            final int ticketStatus = Integer.parseInt(doc.getTransTypeCode());

            final ZipHelper zip = new ZipHelper();
            zip.prepareZip(arcData.zip);
            zip.addFile(doc.getDocGUID() + ".xml", record.getFileBody());
            zip.addFile(doc.getDocGUID() + ".bin", record.getSignBody());
            arcData.zip = zip.generate();

            boolean changeStatus = false;
            if (ticketStatus > arcData.substatus) {
                arcData.substatus = ticketStatus;
                changeStatus = true;
            }

            if (arcData.substatus == 9)
                arcData.uvutext = doc.getUvutext();

            arcDB.saveArchiveData(arcData, changeStatus);
            print("archive data modified successfully");

            // write ticket to FTP for recipient
            final List<Integer> users = dbpro.getFTPUsers(doc.getRecipient());
            for (final int user : users) {
                ArchiveServant.mq.moveToWriter(user, record.getFileName(), "inbox/", 2, record.getFileBodyBase64());
                ArchiveServant.mq.moveToWriter(user, record.getFileName().replaceAll(".xml", ".bin"), "inbox/", 2, record.getSignBodyString());
                print("move to writer for user=" + user);
            }

            ArchiveServant.mq.clear(record.getMqid());

        } catch (Exception e) {
            e.printStackTrace();
            System.exit(0);
        }
    }

    private byte[] createInvexPacket(final DocInfo doc) throws Exception {

        Map<String, String> docdata = new HashMap<>();
        docdata.put("exchangeguid", doc.getExchangeGUID());
        docdata.put("transtypecode", doc.getTransTypeCode());
        docdata.put("transtype", doc.getTransType());
        docdata.put("sender", doc.getSender());
        docdata.put("recipient", doc.getRecipient());
        docdata.put("doctypecode", doc.getDocTypeCode());
        docdata.put("doctype", doc.getDocType());
        docdata.put("docid", doc.getDocGUID());
        docdata.put("docname", doc.getFileName());

        final InvexPackager invexPackager = new InvexPackager();

        doc.setContentFileName(invexPackager.filename);
        doc.setContentSignName(invexPackager.signname);

        final byte[] description = invexPackager.createDescription(docdata);
        return invexPackager.generatePacket(description, record.getFileBody(), record.getSignBody());
    }

    private DocInfo getDocInfo() throws Exception {
        final SOSFile xml = getSosFile(record.getFileBody());
        DocInfo doc = new DocInfo(record.getFileName());

        if (record.getFileName().startsWith("ON_SCHFDOPPOK")) {
            doc.setParentGUID(xml.infpok.updPokInfo.name);
            if (xml.infpok.sodfhzh4.funkciya.equals("���")) {
                doc.setDocType("�������������");
                doc.setDocTypeCode("14");
            }
        }
        else if (record.getFileName().startsWith("DP_IZVPOL"))
            doc.setParentGUID(xml.document.izvpolInfo.relativeFile.name);
        else if (record.getFileName().startsWith("DP_UVUTOCH")) {
            doc.setParentGUID(xml.document.utochInfo.relativeFile.name);
            doc.setUvutext(xml.document.utochInfo.text);
        }

        return doc;
    }

    private SOSFile getSosFile(byte[] content) throws Exception {
        return (SOSFile) xStream.fromXML(new ByteArrayInputStream(content));
    }

    private void print(final String text) {
        final String log = "[" + record.getFileID() + "] " + text;
        Utils.print(log);
    }
}