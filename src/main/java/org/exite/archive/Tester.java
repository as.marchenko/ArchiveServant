package org.exite.archive;

import org.exite.archive.mq.MQRecord;
import org.exite.archive.mq.RedisWorker;
import org.exite.archive.utils.Utils;

import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by alex on 11.08.17.
 */
public class Tester {

    private static Config config;
    private static RedisWorker mq;

    private static MQRecord getRecord() throws Exception {
        final String name = "ON_SCHFDOPPOK_2LD7528BE86-2579-45A3-9D43-AE8B709D8EFE_2LDCF32DD15-2299-4432-B3B3-4E1911F295AC_20170724_B8E37559-5876-D85D-E100-0000C0A8FF5F.xml";
        final byte[] body = Files.readAllBytes(Paths.get("/home/alex/asd/archiveServant/" + name));

        MQRecord record = new MQRecord();
        record.setFileName(name);
        record.setFileBody(body);
        record.setSignBody("sign".getBytes());
        return record;
    }

    public static void main(String[] args) throws Exception {

        if (args.length == 1) {
            config = new Config(args[0]);
            mq = new RedisWorker(Config.redisQueue);

            ArchiveServant.cons.initConnections(Config.poolSize);

            final MQRecord record = getRecord();
            final Worker worker = new Worker(record);
            worker.start();
        } else {
            Utils.print("forgot config file");
        }
    }
}
