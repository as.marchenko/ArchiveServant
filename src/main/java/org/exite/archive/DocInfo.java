package org.exite.archive;

/**
 * Created by alex on 10.08.17.
 */
public class DocInfo {

    private String docGUID;
    private String parentGUID;
    private String exchangeGUID;
    private String transType;
    private String transTypeCode;
    private String docType;
    private String docTypeCode;
    private String sender;
    private String recipient;
    private String fileName;
    private String uvutext;

    private String contentFileName;
    private String contentSignName;

    DocInfo(final String fileName) {
        final String[] parts = fileName.split("_");
        this.docGUID = parts[5].replace(".xml", "");
        this.sender = parts[3];
        this.recipient = parts[2];
        this.fileName = fileName.endsWith(".xml") ? fileName : fileName + ".xml";
        setTypes(parts[1]);
    }

    private void setTypes(final String type) {
        if (type.equals("IZVPOL")) {
            transTypeCode = "06";
            docTypeCode = "06";
            transType = "�������������������������������";
            docType = "�������������������";
        } else if (type.equals("SCHFDOPPOK")) {
            transTypeCode = "11";
            docTypeCode = "13";
            transType = "��������������������";
            docType = "����������������";
        } else if (type.equals("UVUTOCH")) {
            transTypeCode = "09";
            docTypeCode = "07";
            transType = "��������������������������������";
            docType = "����������������������";
        }
    }

    void setParentGUID(final String parentName) {
        final String[] parts = parentName.split("_");
        this.parentGUID = parts[5].replace(".xml", "");
    }

    public void setExchangeGUID(String exchangeGUID) {
        this.exchangeGUID = exchangeGUID;
    }

    public void setDocType(String docType) {
        this.docType = docType;
    }

    public void setDocTypeCode(String docTypeCode) {
        this.docTypeCode = docTypeCode;
    }

    public String getDocGUID() {
        return docGUID;
    }

    public String getParentGUID() {
        return parentGUID;
    }

    public String getExchangeGUID() {
        return exchangeGUID;
    }

    public String getTransType() {
        return transType;
    }

    public String getTransTypeCode() {
        return transTypeCode;
    }

    public String getDocType() {
        return docType;
    }

    public String getDocTypeCode() {
        return docTypeCode;
    }

    public String getSender() {
        return sender;
    }

    public String getRecipient() {
        return recipient;
    }

    public String getFileName() {
        return fileName;
    }

    public String getContentFileName() {
        return contentFileName;
    }

    public void setContentFileName(String contentFileName) {
        this.contentFileName = contentFileName;
    }

    public String getContentSignName() {
        return contentSignName;
    }

    public void setContentSignName(String contentSignName) {
        this.contentSignName = contentSignName;
    }

    public String getUvutext() {
        return uvutext;
    }

    public void setUvutext(String uvutext) {
        this.uvutext = uvutext;
    }

    @Override
    public String toString() {
        return "DocInfo {" +
                "fileName='" + fileName + '\'' +
                ", exchangeGUID='" + exchangeGUID + '\'' +
                '}';
    }
}
