package org.exite.archive.db;

/**
 * Created by alex on 11.08.17.
 */
public class ArchiveData {

    public int substatus;
    public long docid;
    public String uvutext;
    public byte[] zip;

    public ArchiveData(int substatus, long docid, byte[] zip) {
        this.substatus = substatus;
        this.docid = docid;
        this.zip = zip;
    }
}
