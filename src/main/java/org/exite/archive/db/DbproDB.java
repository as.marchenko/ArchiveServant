package org.exite.archive.db;

import org.exite.utils.db.AbstractDataBase;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by alex on 10.08.17.
 */
public class DbproDB extends AbstractDataBase {

    public DbproDB(final String url, final String user, final String pass) throws Exception {
        final String driver = "com.mysql.jdbc.Driver";
        initConnection(driver, url, user, pass);
    }

    public List<Integer> getFTPUsers(final String recipient) throws Exception {

        final String uuid = recipient.replaceFirst("2LD", "");

        final List<Integer> users = new ArrayList<>();
        final String sql = "SELECT intUserID FROM glns2sos gs JOIN users2glns ug ON gs.intGlnID=ug.intGlnID WHERE varGUID='" + uuid + "' AND intSosID=22;";
        try (final PreparedStatement ps = getPreparedStatement(sql);
             final ResultSet rs = ps.executeQuery()) {

            while (rs.next())
                users.add(rs.getInt("intUserID"));
        }
        return users;
    }
}
