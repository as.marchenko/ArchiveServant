package org.exite.archive.db;

import org.exite.archive.Config;
import org.exite.archive.utils.Utils;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * Created by alex on 10.08.17.
 */
public class DbConnectionsPool {

    private BlockingQueue<InvexDB> invexPool;
    private BlockingQueue<ArchiveDB> arcPool;
    private BlockingQueue<DbproDB> dbproPool;

    public void initConnections(final int size) throws Exception {
        invexPool = new ArrayBlockingQueue<>(size);
        arcPool = new ArrayBlockingQueue<>(size);
        dbproPool = new ArrayBlockingQueue<>(size);

        for (int i = 0; i < size; i++) {
            invexPool.add(new InvexDB(Config.invexUrl, Config.invexUser, Config.invexPass));
            arcPool.add(new ArchiveDB(Config.arcUrl, Config.arcUser, Config.arcPass));
            dbproPool.add(new DbproDB(Config.dbproUrl, Config.dbproUser, Config.dbproPass));
        }
        Utils.print("Connections pool created");
    }

    public InvexDB getInvexDB() throws Exception {
        return invexPool.poll(5, TimeUnit.SECONDS);
    }

    public ArchiveDB getArcDB() throws Exception {
        return arcPool.poll(5, TimeUnit.SECONDS);
    }

    public DbproDB getDbproDB() throws Exception {
        return dbproPool.poll(5, TimeUnit.SECONDS);
    }

    public void releaseInvexDB(final InvexDB invexDB) throws Exception {
        invexPool.add(invexDB);
    }

    public void releaseArcDB(final ArchiveDB archiveDB) throws Exception {
        arcPool.add(archiveDB);
    }

    public void releaseDbproDB(final DbproDB dbproDB) throws Exception {
        dbproPool.add(dbproDB);
    }

    public void releaseAll() throws Exception {
        boolean notEmpty = true;
        while (notEmpty) {
            final InvexDB invexDB = getInvexDB();
            final ArchiveDB arcDB = getArcDB();
            final DbproDB dbproDB = getDbproDB();
            if (invexDB == null || arcDB == null || dbproDB == null)
                notEmpty = false;
            else {
                invexDB.close();
                arcDB.close();
                dbproDB.close();
            }
        }
        Utils.print("Connections pool closed");
    }
}
