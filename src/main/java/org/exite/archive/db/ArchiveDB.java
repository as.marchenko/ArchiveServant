package org.exite.archive.db;

import org.exite.utils.db.AbstractDataBase;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Created by alex on 10.08.17.
 */
public class ArchiveDB extends AbstractDataBase {

    public ArchiveDB(final String url, final String user, final String pass) throws Exception {
        final String driver = "com.mysql.jdbc.Driver";
        initConnection(driver, url, user, pass);
    }

    public ArchiveData getArchiveData(final String exchange) throws Exception {
        reconnect();
        final String sql = "SELECT intExiteDocID, varZip, varIndex " +
                "FROM fns_trans_data_zip f JOIN index_intsubstatusid s ON f.intExiteDocID=s.intDocID WHERE varDocExchangeGUID='" + exchange + "' LIMIT 1;";
        try (final PreparedStatement ps = getPreparedStatement(sql);
             final ResultSet rs = ps.executeQuery()) {

            if (rs.next())
                return new ArchiveData(rs.getInt("varIndex"), rs.getLong("intExiteDocID"), rs.getBytes("varZip"));
        }
        return null;
    }

    public void saveArchiveData(final ArchiveData data, boolean changeStatus) throws Exception {
        reconnect();
        final String update_f = "UPDATE fns_trans_data_zip SET varZip=? WHERE intExiteDocID=?;";
        try (PreparedStatement ps = getPreparedStatement(update_f)) {
            ps.setBytes(1, data.zip);
            ps.setLong(2, data.docid);
            ps.executeUpdate();
        }

        if (changeStatus) {
            final String update_s = "UPDATE index_intsubstatusid SET varIndex=? WHERE intDocID=?;";
            try (PreparedStatement ps = getPreparedStatement(update_s)) {
                ps.setInt(1, data.substatus);
                ps.setLong(2, data.docid);
                ps.executeUpdate();
            }
        }

        if (data.uvutext != null && data.uvutext.length() > 0) {
            final String replace = "REPLACE INTO index_vartextuvutoch VALUES ((SELECT intChainID FROM chains_docs WHERE intDocID=? LIMIT 1), ?, ?);";
            try (PreparedStatement ps = getPreparedStatement(replace)) {
                ps.setLong(1, data.docid);
                ps.setLong(2, data.docid);
                ps.setString(3, data.uvutext);
                ps.executeUpdate();
            }
        }
    }
}
