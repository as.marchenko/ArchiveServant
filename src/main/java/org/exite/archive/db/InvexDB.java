package org.exite.archive.db;

import org.exite.archive.DocInfo;
import org.exite.utils.db.AbstractDataBase;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Created by alex on 10.08.17.
 */
public class InvexDB extends AbstractDataBase {

    public InvexDB(final String url, final String user, final String pass) throws Exception {
        final String driver = "com.mysql.jdbc.Driver";
        initConnection(driver, url, user, pass);
    }

    public String getExchangeGUID(final String uuid) throws Exception {
        reconnect();
        try (final PreparedStatement ps = getPreparedStatement("SELECT varInterchId FROM sos_docs WHERE varDocId='" + uuid + "';");
             final ResultSet rs = ps.executeQuery()) {

            if (rs.next())
                return rs.getString("varInterchId");
        }
        return null;
    }

    public long saveInvexTransaction(final DocInfo doc, final byte[] packet) throws Exception {
        reconnect();
        try (PreparedStatement ps = getPreparedStatement("{call save_transaction(?,?,?,?,?,?,?,?,?,?)}")) {
            ps.setString(1, doc.getDocGUID());
            ps.setString(2, doc.getDocType());
            ps.setString(3, doc.getFileName());
            ps.setString(4, doc.getSender());
            ps.setString(5, doc.getRecipient());
            ps.setString(6, doc.getExchangeGUID());
            ps.setString(7, doc.getContentFileName());
            ps.setString(8, doc.getContentSignName());
            ps.setString(9, doc.getTransTypeCode());
            ps.setBytes(10, packet);

            try (final ResultSet rs = ps.executeQuery()) {
                if (rs.next())
                    return rs.getLong("doc_id");
            }
        } catch (Exception ex){
            ex.printStackTrace();
        }
        return 0;
    }

}
